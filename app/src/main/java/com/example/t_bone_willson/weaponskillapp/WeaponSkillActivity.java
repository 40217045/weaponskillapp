package com.example.t_bone_willson.weaponskillapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import java.util.Random;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;


public class WeaponSkillActivity extends AppCompatActivity {

    CharacterClass characterClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weapon_skill);

        //Next 6 instances of code is to identify what UI functions, talk to specific UI ID's.
        final Spinner spin_yws = (Spinner) findViewById(R.id.spin_yws);

        final Spinner spin_tws = (Spinner) findViewById(R.id.spin_tws);

        final Spinner spin_attacks = (Spinner) findViewById(R.id.spin_attacks);

       final TextView tv_wsnvalue = (TextView) findViewById(R.id.tv_wsnvalue);

        final TextView tv_savalue = (TextView) findViewById(R.id.tv_savalue);

        Button btn_roll = (Button) findViewById(R.id.btn_roll);

        btn_roll.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                String YWS = spin_yws.getSelectedItem().toString();
                int intYWS = Integer.parseInt(YWS);

                String TWS = spin_tws.getSelectedItem().toString();
                int intTWS = Integer.parseInt(TWS);

                String A = spin_attacks.getSelectedItem().toString();
                int intA = Integer.parseInt(A);

                //constructor
                characterClass = new CharacterClass(intYWS, intTWS, intA);

                //Takes int value from GetWS() method and turns it into a string. This is so that TextView "tv_wsnvalue" can use that string data to appear
                int wsnvalue = (characterClass.GetWS());
                String WSN = Integer.toString(wsnvalue);
                tv_wsnvalue.setText(WSN);

                //Takes int value from DRoll() method and turns it into a string. This is so that TextView "tv_savalue" can use that string data to appear
                int avalue = (characterClass.DRoll());
                String AV = Integer.toString(avalue);
                tv_savalue.setText(AV);

                //Toast message
                Toast.makeText(WeaponSkillActivity.this, "Rolled!", Toast.LENGTH_SHORT).show();

            }
        });

    }
}
