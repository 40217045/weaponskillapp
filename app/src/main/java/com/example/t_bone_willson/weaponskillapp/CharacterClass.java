package com.example.t_bone_willson.weaponskillapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.util.Random;

public class CharacterClass {

    private int weaponSkill;
    private int theirWeaponSkill;
    private int attacks;

    //Custom Constructor which takes data from the WeaponSkillActivity
    public CharacterClass(int YWS, int TWS, int A) {
        this.weaponSkill = YWS;
        this.theirWeaponSkill = TWS;
        this.attacks = A;
    }

    //Line 20 to 41 is code for getter & setters for the instances of this class
    public int getWeaponSkill() {
        return weaponSkill;
    }

    public void setWeaponSkill(int weaponSkill) {
        this.weaponSkill = weaponSkill;
    }

    public int getTheirWeaponSkill() {
        return theirWeaponSkill;
    }

    public void setTheirWeaponSkill(int theirWeaponSkill) {
        this.theirWeaponSkill = theirWeaponSkill;
    }

    public int getAttacks() {
        return attacks;
    }

    public void setAttacks(int attacks) {
        this.attacks = attacks;
    }

    //method for calculating WS vs WS result
    public int GetWS(){

        //make variable. No idea why, but this allows the correct values to be returned with the nested if statements below. Also set the variable value to 0
        int WSValue = 0;

        // if your WS is equal to their WS
        if (getWeaponSkill() == getTheirWeaponSkill())
        {
            WSValue = 4;
        }

        //If your WS is greater than their WS then return value 3
        else if (getWeaponSkill() > getTheirWeaponSkill())
        {
            //If your WS is greater than or euqal to their WS + 3, return value 2
            if(getWeaponSkill() >= getTheirWeaponSkill() + 3)
            {
                WSValue = 2;
            }
            else
            {
                WSValue = 3;
            }

        }

        //if your WS is less than their WS, return value 4
        else if (getWeaponSkill() < getTheirWeaponSkill()){

            //If your WS + 4 is less than or greater to their WS, return value 6
            if(getWeaponSkill() + 4 <= getTheirWeaponSkill()){
                WSValue = 6;
            }
            //If your WS + 2 is less than or greater to their WS, return value 5
            else if (getWeaponSkill() + 2 <= getTheirWeaponSkill())
            {
                WSValue = 5;
            }
            else
            {
                WSValue = 4;
            }
        }
        //If no WS value inputed, return default value of variable, which is 0
        return WSValue;
    }

    public int DRoll()
    {
        //Declare int totalHits
        int totalHits = 0;
        Random random = new Random();
        //Declare int RandomRoll
        int RandomRoll;
        //For loop. Checks the number of attacks you have and then does the same number of "random rolls" to the attacks value
        for (int x=0; x < getAttacks(); x++)
        {
            //Makes the random dice roll value between 1 to 7 {effectively a 1D6 in computer language}
            RandomRoll = random.nextInt(6 + 1);
            //Checks result of RandomRoll to the WS value needed
            if (RandomRoll >= GetWS())
            {
                //Increments the value of how many attacks hit!
                totalHits++;
            }
        }
        return totalHits;

    }

}
